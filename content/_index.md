---
layout: single
date: 2020-0709
---

# Welcome!

You've sucessfully set up the GitLab run-at-home CTF!

## Howto play

All challenges should be solved **without** inspecting the Docker containers. Just using the description and interacting with the services is sufficient to solve the challenges. Some flags are defined in the `docker-compose.yml` file which is used to run the CTF challenges and this website, so grabbing those flags from the `docker-compose.yml` file is considered cheating too. :D

If you're really stuck have a look at the [spoilers](/spoilers) section where you can find the soultions to the challenges.

## Challenges

Current set of challenges:

* [Sea-Surf](/sea-surf) 1-3
* [tar2zip](/tar2zip)
* GEE CEE M [one](/gee-cee-m) and [two](/gee-cee-m-two)
* [GTP & OTP](/gtp-otp)
* [Tanuki Vault 1-3](/vault)
* Graphicle [One](/graphicle-one) and [two](/graphicle-two)
* [Nyan](/nyan)
* [RST](/rst) and [RST2](/rst2) 

## Flags

The format of the flags is

`[tanuki](random-string)`

Where `random-string` is 32 chars lowercase `0-9a-f`.

Just like this:
```
ruby -rsecurerandom -e 'print "[tanuki](#{SecureRandom.hex(16)})"'
[tanuki](691a6092c752ff2bcab5d68b4853eeb9)
```
