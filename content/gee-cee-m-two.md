---
layout: page
title: Gee-Cee-M Nr. 2
subtitle: Great Crypto, More
---
# AES GCM Challenge II - Description

Can you break the cipher and [encode](http://geeceem.local.thetanuki.io/redeem/goodluck) the following token?

```ruby
class TokenController < ApplicationController
  include CryptoHelper
  def redeem
     decrypted = aes256_gcm_decrypt(params[:token])
     if decrypted == "caughtthetanuki"
       render plain: "FLAG:D"
     else
       render plain: "noot"
     end
     
  end
end
```

The naive solution is not allowed:

```ruby
class User < ApplicationRecord
  before_validation(on: :create) do
    if attribute_present?("token")
      self.token = "noot" if token.match("tanuki") 
    end
  end
before_save      EncryptionWrapper.new
end
```

The route to the TokenController is:

`get 'redeem/:token', to: 'token#redeem'` so bear in mind the URL encoding!
