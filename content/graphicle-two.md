# Graphicle Two

Graphicle is a project management tool with a GraphQL endpoint. The Graphicle
project hosts their issue tracker on the site. They do love to eat their own
dogfood. See if you can find and view a confidential issue in [their public
repo](http://graphicle-two.local.thetanuki.io).


Note: If you need a hint, there might just be an endpoint for that. 😉
