# GTP & OTP 

Those two challenges are to be solved in order. First GTP needs to be done to
get access to the OTP challenge.

## GTP

The `admin` has a password, can you find or guess it and login [here](http://otp.local.thetanuki.io)?
Here's the [source](/frontend.tar.gz), maybe it has some hints.

## OTP

* Requires `GTP` (see above) to be completed

Hooray! You've figured the `admin` password.
But... oh bummer.. `admin` is using a second factor! 
Try to bypass it to solve this challenge. However please do NOT try to brute force the 2nd factor!
Here's the [backend source](/otp.go), it definitely has some hints.
