# RST

[Here](http://rst.local.thetanuki.io) is another password protected area.


Hints:

The admin is `admin@thetanuki.io`.

```ruby
class ResetController < ApplicationController
  def password_reset
    @user = User.where(reset_password_token: params[:token]).first
    if @user
      session[params[:token]] = @user.id
    else
      user_id = session[params[:token]]
      @user = User.find(user_id) if user_id
    end
    if !@user
      render :plain => "no way!"
      return
    elsif params[:password] && @user && params[:password].length > 6
      @user.password = params[:password]
        if @user.save
          render :plain => "password changed ;)"
          return
        end
    end
    render :plain => "error saving password!"
  end
end
```

```ruby
Rails.application.routes.draw do
  devise_for :users
   authenticated :user do
    root to: 'home#index', as: :authenticated_root
  end
  root to: redirect('/users/sign_in')
   get 'reset' => 'reset#password_reset'
end
```

