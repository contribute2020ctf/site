# RST 2

RST has [been fixed](http://rst2.local.thetanuki.io)!

```diff
diff --git a/config/routes.rb b/config/routes.rb
index f854b33..6497ce1 100644
--- a/config/routes.rb
+++ b/config/routes.rb
@@ -5,5 +5,5 @@ Rails.application.routes.draw do
     root to: 'home#index', as: :authenticated_root
   end
   root to: redirect('/users/sign_in')
-   get 'reset' => 'reset#password_reset'
+   get 'reset/:token' => 'reset#password_reset'
 end
```
