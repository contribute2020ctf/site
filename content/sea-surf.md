# Sea-surf Challenges - Description

The Site Security Review Forum loves to judge other sites on their security
headers. It is not a fan of self-reflection though. Can you get the site to
talk to itself?

There are 3 different challenges, each with an increasing level of input url
validation. If you need a hint, please see the `FAQ`.


* [Level 1](http://ssrf1.local.thetanuki.io)
* [Level 2](http://ssrf2.local.thetanuki.io)
* [Level 3](http://ssrf3.local.thetanuki.io)
