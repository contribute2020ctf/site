---
date: 2020-04-07
---
# AES GCM Challenge II

The key and the IV of the AES GCM used to encrypt the token values are static. 
This time we need to encrypt some value and have a fitting auth tag. Luckily
there is a [tool for that](https://github.com/nonce-disrespect/nonce-disrespect).


So this challenge is solvable as follows:

1. Create a user with a token value of e.g. `AAAAAAAAAAAAAAAAAAAAAA`
2. Observe the user's encrypted token value: `MvR02wL0/gz6nryF6RF7JjBiyRdnbkjSS5gbrAhJg79j5ntQgxM=`
3. XOR the string `AAAAAAAAAAAAAAAAAAAAAA` with `Base64.decode64("MvR02wL0/gz6nryF6RF7JjBiyRdnbkjSS5gbrAhJg79j5ntQgxM=")` to obtain the keystream
4. XOR the keystream with the target value `caughtthetanuki`

Now use the nonce-disrespect tool:

```
./recover "" 10d050b737d0da60deb99be9dc315b4a1f56fd7b4d4a58 4d15b977162cef718a00171bb8bdb642 "" 07d046ee71c1da3ecfed89a1db2408 4eb44b1c85a43f7d1160a1c0d6dd6172
363f2d6b46d48724a004a9031d61947b
900104caafd4952afe9bffb09561ced9
ae647af3ad583824bcda82f4298cb48d
 ✘ joern@hostname  ~/nonce-disrespect/tool   master  ./forge "" 10d050b737d0da60deb99be9dc315b4a1f56fd7b4d4a58 4d15b977162cef718a00171bb8bdb642 "" 10d440fd2bc1cb25deab9caadd3b53 363f2d6b46d48724a004a9031d61947b
8e92e09d425ffdc746e4ab2a0e9e1776
 joern@hostname  ~/nonce-disrespect/tool   master  ./forge "" 10d050b737d0da60deb99be9dc315b4a1f56fd7b4d4a58 4d15b977162cef718a00171bb8bdb642 "" 10d440fd2bc1cb25deab9caadd3b53 900104caafd4952afe9bffb09561ced9
75a603869d12555b0da2e15d294404de
 joern@hostname  ~/nonce-disrespect/tool   master  ./forge "" 10d050b737d0da60deb99be9dc315b4a1f56fd7b4d4a58 4d15b977162cef718a00171bb8bdb642 "" 10d440fd2bc1cb25deab9caadd3b53 ae647af3ad583824bcda82f4298cb48d
2505292b8ca80d813185fd3bade8ab6f
```

Now we must concatenat `10d440fd2bc1cb25deab9caadd3b53` with each of the three forge output values (those are the forged auth tags). Each of the three candidates will needs to be base64 encoded and URL encoded. Afterwards we can submit them and see which one fits.
