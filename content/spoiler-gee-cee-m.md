---
date: 2020-04-07
---

# AES GCM Challenge I

The key and the IV of the AES GCM used to encrypt the token values are static. 
As we only need to decipher the admin's token we can exploit the fact that
AES GCM is basically a stream cipher, we don't need to care about the auth
tag within this challenge.

So this challenge is solvable as follows:

1. Create a user with a token value of e.g. `AAAAAAAAAAAAAAAAAAAAAA`
2. Observe the user's encrypted token value: `MvR02wL0/gz6nryF6RF7JjBiyRdnbkjSS5gbrAhJg79j5ntQgxM=`
3. XOR the string `AAAAAAAAAAAAAAAAAAAAAA` with `Base64.decode64("MvR02wL0/gz6nryF6RF7JjBiyRdnbkjSS5gbrAhJg79j5ntQgxM=")` to obtain the keystream
4. XOR `Base64.decode64(`EsZR/CLG2yvjtUlfG1Bw24mwe6G1PUF3`) with the keystream to obtain the admin token

## script

```ruby
#!/usr/bin/ruby

require 'base64'

if ARGV.length != 3 
then
  puts "Usage: #{File.basename(__FILE__)} token_encrypted user_token_encrpted user_token_plain"
  exit -1
end

plain  = ARGV[2]
enc    = Base64.decode64 ARGV[1]
target = Base64.decode64 ARGV[0]

class String
  def ^(second)
   self.length > second.length ? (string2=self;string1=second) : (string1=self;string2=second)
   s = []
   string1.unpack('C*').zip(string2.unpack('C*')) {|a,b| s.push( (a||0) ^ (b||0) ) }
   return s.pack('C*')
  end
end

enc = enc[0..-17] # cut auth tag
target = target [0..-17] # same

keystream = plain ^ enc

p keystream ^ target
```


## Video walk through

{{< youtube r1awgAl90wM>}}
