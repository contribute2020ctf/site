# Graphicle One

The goal of this challenge is to find the `private` project. The `projects`
endpoint only displays `public` projects, but the `project(id)` endpoint does
not have the same validation.

So this challenge is solvable as follows:

1. Query the `projects` endpoint to get a list of known `public` projects.
Make note of the `id` field as those listed cannot be the private project.
2. Query the `project(id)` endpoint with an `id` not in that list and see if
the private project is displayed.
```
query {
    project(id: 4) {
        id
        name
        description
        private
    }
}
```
