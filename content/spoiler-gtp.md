# GTP
Write up by [@cat](https://gitlab.com/cat)

The description mentions that `admin` has a password which we should find/guess (though a hyperlink for find) and a sourcecode attached.

We can download and extract the sourcecode, then do a grep for "admin":

```bash
╰─>$ rg "admin"
config/initializers/assets.rb
14:# Rails.application.config.assets.precompile += %w( admin.js admin.css )

db/seeds.rb
8:User.create(username: 'admin', password: 'admin', email: 'admin@thetanuki.io')
```

Oh well, admin:admin, could have been easily guessed as well. Logging in with that shows the flag directly.
