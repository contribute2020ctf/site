# NYAN

Write up by [@T4cC0re](https://gitlab.com/T4cC0re)

### Recon

This challenge from the `strange` category was exactly that, strange. You are provided with
a link to a website, only containing a headline `NYAN` and a GIF of the [nyan.cat](http://www.nyan.cat/).

When digging in the HTML and JavaScript there were no oddities there, so the target clearly was the GIF.

So my fist step was so look at the GIF with the `file` command, which met me with a whole lot of nothing that I did not know already.

```
$ file nyan.gif
nyan.gif: GIF image data, version 89a, 316 x 200
```

This *is* indeed a valid GIF, no surprises there.

But in the back of my mind I had the idea, that this might be a [poliglot](https://en.wikipedia.org/wiki/Polyglot_%28computing%29).  
So I dug out my trusty hex viewer to see what this is all about.

```
$ xxd nyan.gif

[...]
00013180: 6c6c 6f46 6c61 672f 504b 0102 1403 0a00  lloFlag/PK......
00013190: 0000 0800 746d 4d50 fe9c f0f1 9e01 0000  ....tmMP........
000131a0: c003 0000 2d00 0000 0000 0000 0000 0000  ....-...........
000131b0: a481 9a03 0000 4d45 5441 2d49 4e46 2f6d  ......META-INF/m
000131c0: 6176 656e 2f69 6f2e 7468 6574 616e 756b  aven/io.thetanuk
000131d0: 692f 4865 6c6c 6f46 6c61 672f 706f 6d2e  i/HelloFlag/pom.
000131e0: 786d 6c50 4b01 0214 030a 0000 0008 00db  xmlPK...........
000131f0: 6d4d 5028 6950 b370 0000 0071 0000 0034  mMP(iP.p...q...4
00013200: 0000 0000 0000 0000 0000 00a4 8183 0500  ................
00013210: 004d 4554 412d 494e 462f 6d61 7665 6e2f  .META-INF/maven/
00013220: 696f 2e74 6865 7461 6e75 6b69 2f48 656c  io.thetanuki/Hel
00013230: 6c6f 466c 6167 2f70 6f6d 2e70 726f 7065  loFlag/pom.prope
00013240: 7274 6965 7350 4b05 0600 0000 000a 000a  rtiesPK.........
00013250: 00c1 0200 0045 0600 0000 00              .....E.....
```

My suspicion was confirmed. At the end of the file, I could clearly see what looks like a zip-like file. The `META-INF` and `maven` strings strongly hinted, that this might be a Java executable (a.k.a. `jar`).

### Exploit

```
$ java -jar nyan.gif
That was easy!
The reward is: [tanuki](be0331e515789e4190adcb98b16b516c)!
```

This is very much a polyglot!

NYAN.
