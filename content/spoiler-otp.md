# OTP

Write up by [@cat](https://gitlab.com/cat)

We get an extra `otp.go` of a OTP checking service. Briefly looking over the service, it has a health check endpoint and a verify OTP one

The health endpoint seems to just return a "Status: OK!" message.

The verify OTP endpoint gets the username and code from the parameters, gets the secret for the user from a sqlite database and compares the generated token with the token sent in the parameters. So far, no SQLi possible as the query is parameterised, and we don't have the sqlite db either.

Let's see how this service is getting called from the frontend code. There's a `HTTPOTPVerifier` lib over at `lib/httpotpverifier.rb` defining the following function:

```ruby
def self.check_code(user, code)
  base_uri = BASE_URI # this is set above as ENV['OTP_ENDPOINT'], so out of our control
  path = "/otp/verify/#{user.id}/#{code}"
  HTTParty.get(base_uri + path)
end
```

The `HTTPOTPVerifier` lib is called in the `TfaController` at `app/controllers/tfa_controller.rb`, with the following:

```ruby
code = params[:code]
result = HTTPOTPVerifier.check_code(current_user, code)
```

We need to be logged in already to access this page, so the only thing that remains under our control is the `code` that we submit on that page.

What's interesting is that we only check the HTTP code of the request to the OTP service afterwards:

```ruby
if result.code == 200  
  session["2ndfactor"] = true
  redirect_to '/secret'
  return
else
  session["2ndfactor"] = false
  redirect_to '/'
  return
end
```

We also notice the `TopsecretController` at `app/controllers/topsecret_controller.rb` which returns the flag if we manage to set the above 2ndfactor session variable to true:

```ruby
before_action :ensure_2nd_factor
def secret
  render plain: ENV["FLAG1"]
end
```

Taking a step back, we can only alter the code and we don't have direct access to that endpoint and we need to make it return a status code 200.

We know from above the path is generated as `"/otp/verify/#{user.id}/#{code}"` where we control the code and we have a health endpoint that returns the status 200! Bingo, we can send code as `../../../health`, which would effectively make the path `/health` and return 200, resulting in us getting the flag!
