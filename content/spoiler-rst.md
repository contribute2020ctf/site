# RST
Write up by [@engwan](https://gitlab.com/engwan)

For this challenge, we are given the username `admin@thetanuki.io` and a
password reset route with the controller code:

```ruby
class ResetController < ApplicationController
  def password_reset
    @user = User.where(reset_password_token: params[:token]).first
    if @user
      session[params[:token]] = @user.id
    else
      user_id = session[params[:token]]
      @user = User.find(user_id) if user_id
    end
    if !@user
      render :plain => "no way!"
      return
    elsif params[:password] && @user && params[:password].length > 6
      @user.password = params[:password]
        if @user.save
          render :plain => "password changed ;)"
          return
        end
    end
    render :plain => "error saving password!"
  end
end
```

This is a `GET` route that expects us to pass the `token` and `password`
query parameters.

Just [loading the page](https://rst.thetanuki.io/reset) without any
parameters gives as an interesting message:

```
error saving password!
```

Looking at the code above, this means that `@user` is not `nil` and we're
just lacking the `password` parameter. `User.where(reset_password_token:
params[:token]).first` actually returned the admin user because its
`reset_password_token` is the empty string. This could be a common scenario
where the user didn't request any reset token yet.

Knowing this, we just send a request with a password that we want and we
should get:

```
password changed ;)
```

After changing the password, we can now login with the password we just set
and see the flag:

```
[tanuki](dbf839366dab89fe5a8cb215d4ad887e)
```
