# Tanuki Vault 1
Write up by [@cat](https://gitlab.com/cat)

We're given an `apk` and told that this should/can be done statically, and there are 3 flags.

To "unpack" an APK, we can use `apktool`:

```bash
apktool d com.gitlab.contribute2020ctf.apk
```

This generates a folder with the same name with the resources and everything extracted: `com.gitlab.contribute2020ctf`

First flag, so it's gotta be simple right?

```bash
╰─>$ grep -Ra "tanuki"
[ag[tanuki](083b8863acdac5fa24b8910d933c0bae)Great job! The vault password is also a flag!
```

Looks like it's in `assets/secret.db`! Good enough for now, we'll find out later where is this used.
