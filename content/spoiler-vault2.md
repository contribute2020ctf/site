# Tanuki Vault 2
Write up by [@cat](https://gitlab.com/cat)

Looking over the APK disassembled output (as described in Vault 3) we can notice in the MainActivity that there is a FirebaseRemoteConfig used to get "messageOfTheDay" for display.

Since the RemoteConfig values are public, we can extract the API key, domain and database URL from the values and use those to peek around and see whether there are other values of interest.

I've initially created a quick HTML file to do that, using the Firebase SDK:

```html
<html>
  <head>
    <script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-remote-config.js"></script>
    <script>
     var config = {
       apiKey: "AIzaSyDOSmEolg_x80N09kjuznIaqr8-yBQQIvs",
       authDomain: "contribute2020ctf.firebaseio.com",
       databaseURL: "https://contribute2020ctf.firebaseio.com",
       storageBucket: "contribute2020ctf.appspot.com",
       projectId: "contribute2020ctf",
       appId: "1:450258535404:android:c5d5e9af3b1ac67002b6c9",
     }
     firebase.initializeApp(config);

     var remoteConfig = firebase.remoteConfig();
     remoteConfig.fetchAndActivate()
       .then(() => {
         console.log(remoteConfig.getAll());
       })
       .catch((err) => {
         console.error(err);
       });
    </script>
  </head>
  <body>
    <pre id='data'></pre>
  </body>
</html>
```

Serve that locally with `python -m http.server`, access it at `http://localhost:8000/test.html` and notice a nice shiny object in the console:

```
{
  flag: Kt {_source: "remote", _value: "[tanuki](d8ef28d10801761ca4aa7fa6c0770869)"},
  messageOfTheDay: Kt {_source: "remote", _value: "We hope you're having fun with the GitLab #Contribute2020 CTF!"}
}
```

After the fact, I realized this was the OSINT task, and found [@dcouture](https://gitlab.com/dcouture)'s [blog post](https://blog.deesee.xyz/android/automation/2019/08/03/firebase-remote-config-dump.html) about recovering Firebase Remote Config information through an API call.

Tested the script quickly, same result, either approach works!
