# Tanuki Vault 3
Write up by [@engwan](https://gitlab.com/engwan)

For this challenge, we're given an APK file that contains 3 flags.

From the previous challenge, we found out that the vault password is also a
flag. So we'll try to find that here.

## Decompiling `classes.dex`

To see how the password is being checked, let's first unzip the APK and
decompile the `classes.dex` file to see the Java source code. To do this,
we can use [jadx](https://github.com/skylot/jadx).

Looking at `com/gitlab/contribute2020ctf/FlagVaultActivity.java` we see:

```java
public final native boolean verifyPassword(String str);

...

static {
  System.loadLibrary("vault");
}
```

This tells us that the code for verifying the password isn't here but is
implemented using the Java Native Interface (JNI). The native code is in a
shared library `libvault.so` which we can find in `lib/`.

This makes things a bit harder but we should still be able to decompile
that.

## Decompiling `libvault.so`

For this step, we can use [Ghidra](https://ghidra-sre.org/). In the
`rodata` segment, we can see some interesting variables like `KEY` and
`VAULT_PASSWORD`:

![image: Screen Shot 2020-03-25 at 3.51.48 PM.png](/Screen_Shot_2020-03-25_at_3.51.48_PM.png)

![image: Screen Shot 2020-03-25 at 3.52.19 PM.png](/Screen_Shot_2020-03-25_at_3.52.19_PM.png)

We can try to check what these are:

```ruby
irb(main):007:0> key = '4731744c34625634756c744b3379'
=> "4731744c34625634756c744b3379"
irb(main):008:0> [key].pack('H*')
=> "G1tL4bV4ultK3y"
irb(main):009:0> vault_password =
'1c45152241093f695d594c2d001f26534c780d0367514255447b0a1d77074d29505b3006465f43720b50'
=>
"1c45152241093f695d594c2d001f26534c780d0367514255447b0a1d77074d29505b3006465f43720b50"
irb(main):010:0> [vault_password].pack('H*')
=> "\x1CE\x15\"A\t?i]YL-\x00\x1F&SLx\r\x03gQBUD{\n\x1Dw\aM)P[0\x06F_Cr\vP"
```

The `key` is interesting but the `vault_password` looks like it's
encrypted. It matches the length of our flag though so this is very likely
to be our flag.

## Decrypting `VAULT_PASSWORD`

Next, we see that there is a `decrypt` function in this library:

![image: Screen Shot 2020-03-25 at 4.01.23 PM.png](/Screen_Shot_2020-03-25_at_4.01.23_PM.png))

This looks like a simple XOR, so let's try repeating our `key` so it
matches the length and then do an XOR:

```ruby
irb(main):013:0> key = key * 3
=>
"4731744c34625634756c744b33794731744c34625634756c744b33794731744c34625634756c744b3379"
irb(main):016:0> decrypted = (key.to_i(16) ^
vault_password.to_i(16)).to_s(16)
=>
"5b74616e756b695d28353866336661623834396131653739303039643036396564396632333337393829"
irb(main):017:0> [decrypted].pack('H*')
=> "[tanuki](58f3fab849a1e79009d069ed9f233798)"
```

And there we find our flag: `[tanuki](58f3fab849a1e79009d069ed9f233798)`
