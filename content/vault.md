# Tanuki Vault

Tanuki Vault is an android static analysis challenge (with a touch of OSINT!)
that contains 3 flags. You can install the application on a phone or an
emulator and it might help to visualize the whole thing if you do, but it isn't
required.

Download the app [here](/com.gitlab.contribute2020ctf.apk)
